package optional

type Optional interface {
	// Generic for both Maybe and MaybeSeq
	Get()
	Failed()
	Succeeded()

	// Maybe
	New()
	Map()
	AndThen()

	// MaybeSeq
	NewSeq()
	MapSeq()
	AndThenSeq()
}

// Maybe

type Maybe[T any] struct {
	Value T
	Error error
}

type Option[T any] func(m *Maybe[T])

func New[T any](options ...Option[T]) *Maybe[T] {
	maybe := &Maybe[T]{}

	for _, opt := range options {
		opt(maybe)
	}

	return maybe
}

func WithValue[T any](t T) Option[T] {
	return func(m *Maybe[T]) {
		m.Value = t
	}
}

func WithError[T any](e error) Option[T] {
	return func(m *Maybe[T]) {
		m.Error = e
	}
}

func (m Maybe[T]) Get(def T) T {
	if m.Error != nil {
		return def
	} else {
		return m.Value
	}
}

func (m Maybe[T]) Failed() bool {
	return m.Error != nil
}

func (m Maybe[T]) Succeeded() bool {
	return m.Error == nil
}

func Map[S any, T any](m *Maybe[S], f func(S) (T, error)) *Maybe[T] {
	if m.Error != nil {
		var unit T
		return New(WithValue(unit), WithError[T](m.Error))
	} else {
		r, err := f(m.Value)
		return New(WithValue(r), WithError[T](err))
	}
}

func (m *Maybe[T]) AndThen(f func(T) (T, error)) *Maybe[T] {
	return Map(m, f)
}

// MaybeSeq

type MaybeSeq[T any] struct {
	Value []T
	Error error
}

type OptionSeq[T any] func(m *MaybeSeq[T])

func NewSeq[T any](options ...OptionSeq[T]) *MaybeSeq[T] {
	maybeSeq := &MaybeSeq[T]{}

	for _, opt := range options {
		opt(maybeSeq)
	}
	return maybeSeq
}

func WithValueSeq[T any](t []T) OptionSeq[T] {
	return func(m *MaybeSeq[T]) {
		m.Value = t
	}
}

func WithErrorSeq[T any](e error) OptionSeq[T] {
	return func(m *MaybeSeq[T]) {
		m.Error = e
	}
}

func (m MaybeSeq[T]) Get(def []T) []T {
	if m.Error != nil {
		return def
	} else {
		return m.Value
	}
}

func (m MaybeSeq[T]) Failed() bool {
	return m.Error != nil
}

func (m MaybeSeq[T]) Succeeded() bool {
	return m.Error == nil
}

func MapSeq[S any, T any](xs *MaybeSeq[S], f func(S) (T, error)) *MaybeSeq[T] {
	if xs.Failed() {
		var unit []T
		return NewSeq(WithValueSeq(unit), WithErrorSeq[T](xs.Error))
	}

	var result []T
	for _, i := range xs.Value {
		r, err := f(i)
		if err != nil {
			return NewSeq(WithValueSeq(result), WithErrorSeq[T](err))
		}
		result = append(result, r)
	}
	return NewSeq(WithValueSeq(result))
}

func (m *MaybeSeq[T]) AndThenSeq(f func(T) (T, error)) *MaybeSeq[T] {
	return MapSeq(m, f)
}
