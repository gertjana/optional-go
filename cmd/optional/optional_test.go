package optional

import (
	"errors"
	"fmt"
	"reflect"
	"testing"

	tu "gitlab.com/gertjana/optional-go/internal/testutils"
)

var f = func(x int) (int, error) { return x + 1, nil }

func TestOptional(t *testing.T) {
	var maybe = New(WithValue(42))
	tu.Equals(t, maybe.Get(0), 42)
	tu.Assert(t, !maybe.Failed(), "%v should succeed", maybe)
	tu.Ok(t, maybe.Error)

	tu.Equals(t, Map(maybe, f).Value, 43)

	var maybeNot = New(WithValue(42), WithError[int](errors.New("multiple 6 by 9 is 42")))

	tu.Equals(t, maybeNot.Get(0), 0)
	tu.NotOk(t, maybeNot.Error)
	tu.Assert(t, !maybeNot.Succeeded(), "%v should fail", maybeNot)

	tu.NotOk(t, (*Map(maybeNot, f)).Error)
}

func TestOptionalSeq(t *testing.T) {
	var seq = []int{1, 2, 3}
	var maybeSeq = NewSeq(WithValueSeq(seq))

	tu.Equals(t, maybeSeq.Get([]int{}), seq)
	tu.Ok(t, maybeSeq.Error)
	tu.Assert(t, maybeSeq.Succeeded(), "%v should succeed", maybeSeq)

	f := func(x int) (int, error) { return x + 1, nil }

	tu.Assert(t, reflect.DeepEqual(MapSeq(maybeSeq, f).Value, []int{2, 3, 4}), "%v should be[]int{2,3,4}", maybeSeq)

	var maybeNotSeq = NewSeq(WithValueSeq(seq), WithErrorSeq[int](errors.New("multiple 6 by 9 is not 42")))

	tu.Equals(t, maybeNotSeq.Get([]int{42}), []int{42})
	tu.NotOk(t, maybeNotSeq.Error)
	tu.Assert(t, !maybeNotSeq.Succeeded(), "%v should fail", maybeNotSeq)
	tu.NotOk(t, (*MapSeq(maybeNotSeq, f)).Error)

}

func TestMapWithDifferentOutputType(t *testing.T) {

	f := func(x int) (string, error) { return fmt.Sprint(x), nil }

	var maybe = New(WithValue(1))

	res := Map(maybe, f)

	tu.Ok(t, res.Error)
	tu.Equals(t, "1", res.Value)

	f2 := func(x int) (string, error) { return "", errors.New("all your base are belong to us") }
	var maybeNot = New(WithValue(1))
	res2 := Map(maybeNot, f2)
	tu.NotOk(t, res2.Error)

}
func TestMapSeqWithDifferentOutputType(t *testing.T) {

	f := func(x int) (string, error) { return fmt.Sprint(x), nil }

	maybe := NewSeq(WithValueSeq([]int{1, 2}))

	res := MapSeq(maybe, f)

	tu.Ok(t, res.Error)
	tu.Equals(t, []string{"1", "2"}, res.Value)

	f2 := func(x int) (string, error) { return "", errors.New("all your base are belong to us") }
	var maybeNot = NewSeq(WithValueSeq([]int{1, 2}))
	res2 := MapSeq(maybeNot, f2)
	tu.NotOk(t, res2.Error)

}

func TestAndThen(t *testing.T) {
	f := func(x int) (int, error) { return x + 1, nil }

	maybe := *New(WithValue(42))

	res := maybe.
		AndThen(f).
		AndThen(f)

	tu.Ok(t, res.Error)
	tu.Equals(t, 44, res.Value)
}

func TestAndThenSeq(t *testing.T) {
	f := func(x int) (int, error) { return x + 1, nil }

	maybe := *NewSeq(WithValueSeq([]int{1, 2}))

	res := maybe.AndThenSeq(f).AndThenSeq(f)

	tu.Ok(t, res.Error)
	tu.Equals(t, []int{3, 4}, res.Value)
}

func TestOneTypeToAnother(t *testing.T) {

	tu.Equals(t, 1, 1)

	// var divideByThree = func(n int) (float64, error) { return float64(n) / 3.0, nil }
	// var toString2Decimals = func(s float64) (string, error) { return fmt.Sprint("%0.2f", s), nil }
	// var maybe Maybe[int] = Maybe{Value: 10, Error: nil}
	// var res string = maybe.Map(divideByThree).Map(toString2Decimals).Value
	// res = "3.33"
}

func BenchmarkMaybe(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var f = func(x int) (int, error) { return x + 1, nil }
		var maybe = New(WithValue(42))
		mapped := Map(maybe, f)
		mapped.AndThen(f).AndThen(f)
	}
}
